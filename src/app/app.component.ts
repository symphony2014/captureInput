import { Component, OnInit, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
interface User {
  firstName: string;
  lastName: string;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnChanges {

  noForm = {
    a: 1,
    b: 2
  };
  constructor(private httpClient: HttpClient) {
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }
  ngOnInit(): void {
    console.log('start. counting....');
    // this.httpClient.get('http://12.22.32.1').subscribe(() => {}, res => {

    //   this.test3 = 'async changed 1';
    //   console.log('finished in 2300ms');
    // });
    // this.httpClient.get('http://www.baidu.com').subscribe(() => {}, res => {
    //   console.log('finished in 5000ms');
    //   this.test3 = 'async changed 2';
    // });
      // setTimeout(() => {
      //   this.test4 = 'async changed';
      // }, 50000);
  }


onSubmit() {

  }
}
