import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef, OnChanges, SimpleChanges, APP_BOOTSTRAP_LISTENER, ComponentRef } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReactiveFormDemoComponent } from './reactive-form-demo/reactive-form-demo.component';
import { AppRoutingModule } from './app-routing.module';
import { OneComponent } from './one/one.component';
import { HttpClientModule } from '@angular/common/http';
import { NormalFormComponent } from './normal-form/normal-form.component';
import { ReactFormComponent } from './react-form/react-form.component';
import { CaptureModule } from './capture/capture.module';

@NgModule({
  declarations: [
     AppComponent,
     ReactiveFormDemoComponent,
     OneComponent,
     NormalFormComponent,
     ReactFormComponent,
  ],
  imports: [
     CaptureModule,
     FormsModule,
     BrowserModule,
     AppRoutingModule,
     ReactiveFormsModule,
     HttpClientModule
  ],
  providers: [{
    provide: APP_BOOTSTRAP_LISTENER, multi: true, useFactory: () => {
      return (component: ComponentRef<any>) => {
        console.log(component.instance);
      };
    }
  }],
  bootstrap: [AppComponent]
})
export class AppModule implements OnChanges  {
constructor(private appRef: ApplicationRef ) {
 console.log(appRef);
}
  ngOnChanges(changes: SimpleChanges): void {
    throw new Error('Method not implemented.');
  }
}
