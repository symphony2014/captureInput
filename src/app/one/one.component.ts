import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.css']
})
export class OneComponent implements OnInit {

  constructor(private httpClient: HttpClient) {}
  noForm = {
    a: 1,
    b: 2
  };
  test: any;
  ngOnInit(): void {
      this.httpClient.get('http://www.baidu.com').subscribe(() => {}, res => {
      console.log('finished in 5000ms');
      this.noForm.a = 2;
    });
      setTimeout(() => {
        this.noForm.b = 222;
      }, 11000);
      setTimeout(() => {
       this.test ='test' 
      },  500);
  }
}
