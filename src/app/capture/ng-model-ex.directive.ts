
import { Directive, ElementRef, forwardRef, Host, Inject, Input, OnChanges, OnInit, Optional, SimpleChanges } from '@angular/core';
import { ControlContainer, NgControl, NgModel } from '@angular/forms';
import { DEFAULT_LIST } from './capture.module';
import { defaultList } from './Global';
export const formControlBinding: any = {
  provide: NgControl,
  useExisting: forwardRef(() => NgModel)
};
@Directive({
  selector: '[ngModelOptions]',
  providers: [formControlBinding],
  exportAs: 'ngModelEx'
})
export class NgModelExDirective implements OnChanges , OnInit {
   // @Inject(DEFAULT_LIST) private defaultList: Array<ElementRef>
   @Input('ngModelOptions') options!: {name?: string, standalone?: boolean};
   @Input() name!: string;
   @Input('ngModel') model: any;
  constructor(@Optional() @Host() parent: ControlContainer ) {
    console.log('constructor');
    console.log(this.name);
  }
  ngOnChanges(changes: SimpleChanges) {
      console.log(changes);
  }
  ngOnInit() {
    console.log(this.options);
    if (this.options.standalone) {
      defaultList.set( this.name,  this);
    }
  }

}
