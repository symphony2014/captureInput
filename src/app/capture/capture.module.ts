import { ElementRef, InjectionToken, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaptureGroupDirective } from './capture-group.directive';
import { NgModelExDirective } from './ng-model-ex.directive';


export const  DEFAULT_LIST = new InjectionToken<Array<ElementRef>>('defaultList');
@NgModule({
  declarations: [CaptureGroupDirective, NgModelExDirective],
  providers: [{
    provide: DEFAULT_LIST,
    useValue:  []
  }],
  imports: [
    CommonModule
  ],
  exports: [
    CaptureGroupDirective, NgModelExDirective
  ]
})
export class CaptureModule {  }
