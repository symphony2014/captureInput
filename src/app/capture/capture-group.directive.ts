
import { Directive, ElementRef,  OnInit, Injector, HostListener, Input, NgZone } from '@angular/core';
import { NgForm, FormGroupDirective } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, RouterOutlet, Router } from '@angular/router';
import diff from 'deep-diff';
import { defaultList } from './Global';
const enum CaptureType {
  ReactiveForm,
  TemplateForm,
  NoForm
}
interface CaptureFilter {
  url: string;
  fields: Array<string>;
  defaults: Array<string>;
}
// Global activity filter.
// export const ACTIVITY_FILTERS: Array<CaptureFilter> = [{url: '/mar/billboards/add/wizard/DIGITAL', fields: ['billboardDetails.latitude', 'billboardDetails.longitude', 'billboardDetails.group']}, {url: '/lmx/expert', fields: ['latitude', 'longitude']}];

// export const ACTIVITY_FILTERS: Array<CaptureFilter> = [{url: '/mar/billboards/add/wizard/DIGITAL', fields: ['billboardDetails.latitude', 'billboardDetails.longitude', 'billboardDetails.group', 'country.name']}];
export const ACTIVITY_FILTERS: Array<CaptureFilter> = [{url: '/mar/billboards/add/wizard/DIGITAL', fields: ['billboardDetails.latitude', 'billboardDetails.longitude', 'billboardDetails.group'], defaults: ['country.name']}];
// Global activity records.
export const ACTIVITY_RECORDS = [];

@Directive({
  selector: 'button,form'
})

export class CaptureGroupDirective implements  OnInit {

  oldValues = {};
  activity: {url: string, fields: Array<string>} = {url: '', fields: []};
  nodeName = '';
  sub: Subscription;
  subForNoForm: Subscription;
  formRef: any;
  type: CaptureType = CaptureType.TemplateForm;
  router: any;
  routerurl: any;
  isCapture: boolean;

  @HostListener('click') click($event) {
    // Only for No Form case.

      console.log('capturing');
      console.log(this.type !== CaptureType.NoForm);
      console.log(!this.router);
      // if (this.type !== CaptureType.NoForm) {
      //   return;
      // }

      if (!this.router) {
        return;
      }
      console.log('capturing bfr');
      const newValues = this.getNewByActivitFilter((this.router as any).activated.instance);
      console.log('  ' + newValues);
      this.saveByActivit(this.oldValues, newValues);

  }

  constructor(  private ref: ElementRef, private ngZone: NgZone, private injector: Injector, private router2: Router, private route: ActivatedRoute) {


    this.router =  injector.get(RouterOutlet, null);
    // this.routerurl = this.route.routeConfig.path;
    this.routerurl = (route.snapshot as any)._routerState.url;
    console.log('routerurl >> Updated ' + this.routerurl);
    if (this.route.component) {
        this.activity.url = (route as any).snapshot._routerState.url;
      }
    this.formRef = injector.get(NgForm, null);
    if (!this.formRef) {
          this.formRef = injector.get(FormGroupDirective, null);
          this.type = CaptureType.ReactiveForm;
          if (this.formRef) {
            return;
          }
      }
    this.nodeName = ref.nativeElement.nodeName.toLowerCase();
    if (this.nodeName === 'button') {
        this.type = CaptureType.NoForm;
        return;
      }


  }
  ngOnInit(): void {
      this.saveOldValues();
      console.log(this.nodeName);
      if (this.type !== CaptureType.NoForm) {
        this.formRef.ngSubmit.subscribe(vals => {
          for (const [name, ngModelEx] of defaultList) {
            this.formRef.value[name] = ngModelEx.model;
          }
          this.saveByActivit(this.oldValues, this.getNewByActivitFilter(this.formRef.value));
        });
      }
  }
  private saveByActivit(oldValues, newValues) {
    const result = diff(oldValues, newValues);
    if (!result || !result.length) { return; }
    console.log('Calling save by activity');
    console.log(this.activity.url);
    console.log({url: this.activity.url, changes: result.map(r => ({field: r.path[0], oldValue: r.lhs, newValue: r.rhs}))});
    ACTIVITY_RECORDS.push({url: this.activity.url, result});
    console.log(ACTIVITY_RECORDS);
  }
  private saveOldValues() {
    switch (this.type) {
      case CaptureType.ReactiveForm:
        this.saveOldByActivitFilter(this.formRef.value);
        break;
      case CaptureType.TemplateForm:

        this.sub = this.ngZone.onStable.subscribe(res => {
          console.log('hit');
          if (!this.ngZone.hasPendingMacrotasks && !this.ngZone.hasPendingMicrotasks) {
            console.log('real stable');
            console.log('hasPendingMacrotasks', this.ngZone.hasPendingMacrotasks);
            console.log('hasPendingMicrotasks', this.ngZone.hasPendingMicrotasks);

            this.saveOldByActivitFilter(this.formRef.value);
            this.sub.unsubscribe();
          }
        });
        break;
      default:
        if (!this.router) {
          return;
        }
// Init Code
        this.subForNoForm = this.ngZone.onStable.subscribe(res => {
          console.log('no form:');
          if (!this.ngZone.hasPendingMacrotasks && !this.ngZone.hasPendingMicrotasks) {
            console.log('real stable');
            console.log('hasPendingMacrotasks', this.ngZone.hasPendingMacrotasks);
            console.log('hasPendingMicrotasks', this.ngZone.hasPendingMicrotasks);

            const oldValuesRaw = (this.router as any).activated.instance;
            console.log(oldValuesRaw);
            this.saveOldByActivitFilter(oldValuesRaw);
            this.subForNoForm.unsubscribe();
          }
        });

        break;
    }
  }

  private saveOldByActivitFilter(originValues) {
    const currentActivit = ACTIVITY_FILTERS.find(a => a.url.match(this.activity.url));
    if (currentActivit && currentActivit.fields.length) {
      if (currentActivit.fields[0] === '*') {
        this.oldValues = JSON.parse(JSON.stringify(this.decycle(originValues)));
        return;
      }
      for (let i = 0; i < currentActivit.fields.length; i++) {
        this.oldValues[currentActivit.fields[i]] = this.mapPath(currentActivit.fields[i] , originValues);
      }
    }
  }

  private decycle(obj, stack = []) {
      if (!obj || typeof obj !== 'object') {
        return obj;
      }
      if (stack.includes(obj)) {
        return null;
      }
      const s = stack.concat([obj]);
      return Array.isArray(obj)
        ? obj.map(x => this.decycle(x, s))
        : Object.keys(obj).sort().reduce((acc, cur) => ({ ...acc, [cur]: this.decycle(cur, s) }), {});
    /*Object.fromEntries(
          Object.entries(obj)
            .map(([k, v]) => [k, this.decycle(v, s)]));*/
    }


  private getNewByActivitFilter(newValuesRaw) {
    const newValues = {};
    const currentActivit = ACTIVITY_FILTERS.find(a => a.url.match(this.activity.url));
    if (currentActivit && currentActivit.fields.length) {

      if (currentActivit.fields[0] === '*') { return newValuesRaw; }

      for (let i = 0; i < currentActivit.fields.length; i++) {
        const field = currentActivit.fields[i];
        newValues[field] = this.mapPath(field , newValuesRaw);
      }
    }
    return newValues;
  }

  private mapPath(pattern: String, obj: any) {
    const path = pattern.split('.');
    let final = obj;
    for (let i = 0; i < path.length; i++) {
      final = final[path[i]];
    }
    return final;
  }
}
