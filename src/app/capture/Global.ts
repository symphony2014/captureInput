import { ComponentRef, ElementRef } from '@angular/core';
import { NgModelExDirective } from './ng-model-ex.directive';

export const defaultList: Map<string, NgModelExDirective> = new Map();
