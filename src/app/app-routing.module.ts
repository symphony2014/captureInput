import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NormalFormComponent } from './normal-form/normal-form.component';
import { OneComponent } from './one/one.component';
import { ReactFormComponent } from './react-form/react-form.component';


const routes: Routes = [{
  path: 'one', component: OneComponent,
}, {
  path: 'normal-form', component: NormalFormComponent,
}, {
  path: 'react-form', component: ReactFormComponent,
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
